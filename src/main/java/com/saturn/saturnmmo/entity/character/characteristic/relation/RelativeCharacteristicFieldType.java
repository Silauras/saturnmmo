package com.saturn.saturnmmo.entity.character.characteristic.relation;


public enum RelativeCharacteristicFieldType {
    MAX_VALUE,
    MIN_VALUE,
    CURRENT_VALUE,
}
