package com.saturn.saturnmmo.entity.character.characteristic.relation;

import com.saturn.saturnmmo.entity.character.characteristic.Characteristic;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@Getter
@EqualsAndHashCode
public class RelativeData {
    private RelativeCharacteristicFieldType changedCharacteristicField;
    private RelativeCharacteristicFieldType relativeCharacteristicField;
    private Characteristic relativeCharacteristic;
    private double changeValuePerPoint;
}
