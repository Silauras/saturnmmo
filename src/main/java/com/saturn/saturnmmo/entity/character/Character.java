package com.saturn.saturnmmo.entity.character;

import com.saturn.saturnmmo.entity.character.characteristic.Characteristic;
import com.saturn.saturnmmo.entity.level.Location;
import com.saturn.saturnmmo.util.math.Vector;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.List;
import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
public class Character {
    private final UUID id;
    private final String name;
    private final String characterType;
    private final List<Characteristic> dynamicCharacteristics;// health,mana, stamina...
    private final List<Characteristic> staticCharacteristics;// strength,intelligence
    private final Location location;
    private final Vector position;
}
