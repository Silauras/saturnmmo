package com.saturn.saturnmmo.entity.character.characteristic;

import com.saturn.saturnmmo.entity.character.characteristic.exception.CharacteristicException;
import com.saturn.saturnmmo.entity.character.characteristic.relation.RelativeCharacteristicFieldType;
import com.saturn.saturnmmo.entity.character.characteristic.relation.RelativeData;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.saturn.saturnmmo.entity.character.characteristic.relation.RelativeCharacteristicFieldType.*;

@Builder
@Getter
@EqualsAndHashCode
public class Characteristic {
    private String name;
    private String description;
    private boolean isStatic;
    private int maxValue;
    private int minValue;
    private int currentValue;

    private final Map<RelativeCharacteristicFieldType, List<RelativeData>> relativeCharacteristic = new HashMap<>();

    public void changeValue(int value) {
        if (this.currentValue + value > minValue
                && this.currentValue + value < maxValue) {
            this.currentValue += value;
        }
        updateRelative(CURRENT_VALUE, value);
    }

    public void changeMinValue(int value) {
        if (isStatic) {
            throw new CharacteristicException("characteristic_001", "Can't update min value for static characteristic");
        }
        this.minValue += value;
        updateRelative(MIN_VALUE, value);
    }

    public void changeMaxValue(int value) {
        if (isStatic) {
            throw new CharacteristicException("characteristic_001", "Can't update max value for static characteristic");
        }
        this.maxValue += value;
        updateRelative(MAX_VALUE, value);
        updateCurrentValue(value);
    }

    public void addRelation(RelativeData relativeData) {
        RelativeCharacteristicFieldType changedCharacteristicField = relativeData.getChangedCharacteristicField();
        if (!relativeCharacteristic.containsKey(changedCharacteristicField)) {
            relativeCharacteristic.put(changedCharacteristicField, new ArrayList<>());
        }
        relativeCharacteristic.get(changedCharacteristicField).add(relativeData);
    }

    private void updateRelative(RelativeCharacteristicFieldType type, int value) {
        if (!relativeCharacteristic.containsKey(type)) return;
        List<RelativeData> relativeData = relativeCharacteristic.get(type);
        relativeData.forEach(data -> {
            switch (data.getRelativeCharacteristicField()) {
                case MAX_VALUE:
                    data.getRelativeCharacteristic().changeMaxValue((int) (value * data.getChangeValuePerPoint()));
                    break;
                case CURRENT_VALUE:
                    data.getRelativeCharacteristic().changeValue((int) (value * data.getChangeValuePerPoint()));
                    break;
                case MIN_VALUE:
                    data.getRelativeCharacteristic().changeMinValue((int) (value * data.getChangeValuePerPoint()));
                    break;
            }
        });
    }

    private void updateCurrentValue(int value) {
        int updateCurrentValue = this.currentValue * maxValue / (maxValue - value);
        if (updateCurrentValue <= minValue) {
            updateCurrentValue = minValue + 1;
        }
        this.changeValue(updateCurrentValue - this.currentValue);
    }

}
