package com.saturn.saturnmmo.entity.character.inventory.item;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ItemType {
    private String type;
}
