package com.saturn.saturnmmo.entity.character.inventory;

import com.saturn.saturnmmo.entity.character.inventory.item.Item;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;
@Builder
@Getter
@EqualsAndHashCode
public class Bag {
    private Integer size;
    private HashMap<String, List<Item>> items;
}
