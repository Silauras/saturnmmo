package com.saturn.saturnmmo.entity.character.inventory;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.List;
@Builder
@Getter
@EqualsAndHashCode
public class Inventory {
    private List<Bag> bag;
    private Equipment equipment;
}
