package com.saturn.saturnmmo.entity.character.inventory.item;

import com.saturn.saturnmmo.entity.character.inventory.item.Item;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Barter {
    private Integer count;
    private Item item;
}
