package com.saturn.saturnmmo.entity.character.inventory.item;

import java.util.Map;
import java.util.UUID;

public class Item {
    private UUID id;
    private String name;
    private ItemType type;
    private Barter buyPrice;
    private Barter sellPrice;
    private String description;
    private Integer weight;
    private Map<String, Object> data;
}
