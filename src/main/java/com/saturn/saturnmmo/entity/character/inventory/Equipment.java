package com.saturn.saturnmmo.entity.character.inventory;

import com.saturn.saturnmmo.entity.character.inventory.item.Item;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.List;
import java.util.Map;

@Builder
@Getter
@EqualsAndHashCode
public class Equipment {
    private Map<EquipmentStand,Item> items;

    public interface EquipmentStand {
    }
}
