package com.saturn.saturnmmo.entity.character.characteristic.exception;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class CharacteristicException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private static final String PARAM = "param";

    private final String code;

    private final String message;

    private final Map<String, String> paramMap = new HashMap<>();

    public CharacteristicException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public CharacteristicException(String code, String message, Map<String, String> paramMap) {
        super(message);
        this.code = code;
        this.message = message;
        this.paramMap.putAll(paramMap);
    }

    public CharacteristicException withParams(String... params) {
        if (params != null && params.length > 0) {
            for (int i = 0; i < params.length; i++) {
                paramMap.put(PARAM + i, params[i]);
            }
        }
        return this;
    }

    @Override
    public String toString() {
        return "{code=" + code + ", message=" + message + (paramMap.isEmpty() ? "" : ", paramMap=" + paramMap) + "}";
    }
}
