package com.saturn.saturnmmo.util.math;

public interface Vector {

    int getX();
    int getY();
    int getZ();

    void setX();
    void setY();
    void setZ();
}
