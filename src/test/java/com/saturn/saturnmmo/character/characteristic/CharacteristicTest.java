package com.saturn.saturnmmo.character.characteristic;


import com.saturn.saturnmmo.entity.character.characteristic.Characteristic;
import com.saturn.saturnmmo.entity.character.characteristic.exception.CharacteristicException;
import com.saturn.saturnmmo.entity.character.characteristic.relation.RelativeData;
import org.junit.jupiter.api.Test;

import static com.saturn.saturnmmo.entity.character.characteristic.relation.RelativeCharacteristicFieldType.CURRENT_VALUE;
import static com.saturn.saturnmmo.entity.character.characteristic.relation.RelativeCharacteristicFieldType.MAX_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CharacteristicTest {

    public static final String TEST_TEXT = "test";
    public static final int TEST_CURRENT_VALUE = 10;
    public static final int TEST_MIN_VALUE = 0;
    public static final int TEST_MAX_VALUE = 100;
    public static final int STANDARD_TEST_VALUE_STEP = 10;
    public static final int BIG_TEST_VALUE_STEP = 90;

    @Test
    public void testInvalidMaxValueUpdate() {
        Characteristic characteristic = buildTestCharacteristic(true);
        CharacteristicException exception = assertThrows(CharacteristicException.class,
                () -> characteristic.changeMaxValue(STANDARD_TEST_VALUE_STEP));
        assertEquals("Can't update max value for static characteristic", exception.getMessage());
    }

    @Test
    public void testInvalidMinValueUpdate() {
        Characteristic characteristic = buildTestCharacteristic(true);
        CharacteristicException exception = assertThrows(CharacteristicException.class,
                () -> characteristic.changeMinValue(STANDARD_TEST_VALUE_STEP));
        assertEquals("Can't update min value for static characteristic", exception.getMessage());
    }

    @Test
    public void testValidMaxValueUpdate() {
        Characteristic characteristic = buildTestCharacteristic(false);
        characteristic.changeMaxValue(STANDARD_TEST_VALUE_STEP);
        assertEquals(TEST_MAX_VALUE + STANDARD_TEST_VALUE_STEP, characteristic.getMaxValue());
    }

    @Test
    public void setTestCurrentValueUpdateAfterMaxValueUpdate() {
        Characteristic characteristic = buildTestCharacteristic(false);
        characteristic.changeMaxValue(-BIG_TEST_VALUE_STEP);
        assertEquals(1, characteristic.getCurrentValue());
        characteristic.changeMaxValue(BIG_TEST_VALUE_STEP);
        assertEquals(10, characteristic.getCurrentValue());
    }

    @Test
    public void testValidMinValueUpdate() {
        Characteristic characteristic = buildTestCharacteristic(false);
        characteristic.changeMinValue(STANDARD_TEST_VALUE_STEP);
        assertEquals(TEST_MIN_VALUE + STANDARD_TEST_VALUE_STEP, characteristic.getMinValue());
    }

    @Test
    public void testRelationUpdate() {
        Characteristic relativeCharacteristic = buildTestCharacteristic(false);
        Characteristic characteristic = buildTestCharacteristic(false, RelativeData.builder()
                .changedCharacteristicField(CURRENT_VALUE)
                .relativeCharacteristicField(MAX_VALUE)
                .changeValuePerPoint(10)
                .relativeCharacteristic(relativeCharacteristic)
                .build());
        characteristic.changeValue(STANDARD_TEST_VALUE_STEP);
        assertEquals(200, relativeCharacteristic.getMaxValue());
    }

    private Characteristic buildTestCharacteristic(boolean isStatic, RelativeData... relativeData) {
        Characteristic characteristic = Characteristic.builder()
                .isStatic(isStatic)
                .name(TEST_TEXT)
                .description(TEST_TEXT)
                .currentValue(TEST_CURRENT_VALUE)
                .maxValue(TEST_MAX_VALUE)
                .minValue(TEST_MIN_VALUE)
                .build();
        for (RelativeData data : relativeData) {
            characteristic.addRelation(data);
        }
        return characteristic;
    }
}
